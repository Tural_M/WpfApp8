﻿using System.Collections.Generic;


namespace WpfApp8
{
  public  class Users
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
       private static List<Users> _userList = new List<Users>();
        public Users(string name,string surname,string email,string password,string username)
        {
            Name = name;
            Surname = surname;
            Password = password;
            Email = email;
            Username = username;
        }
        public static void Add(Users u)
        {
            _userList.Add(u);
        }
        public static List<Users> GetUsers()
        {
            if (_userList.Count == 0)
            {
                return null;
            }
            else return _userList;
        }
    }
}
