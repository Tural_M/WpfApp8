﻿using System.Collections.Generic;
using System.Windows;


namespace WpfApp8
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Interaction logic for UserLog.xaml
    /// </summary>
    public partial class UserLog 
    {
        public UserLog()
        {
            InitializeComponent();
        }

        private void Submitbtn_Click(object sender, RoutedEventArgs e)
        {
            if (Users.GetUsers() == null)
            {
                MessageBox.Show("User Not Found!");
                return;
            }

            foreach (var item in Users.GetUsers())
            {
                if (!item.Username.Equals(Logtxtbox.Text)) continue;
                if (item.Password.Equals(PastxtBox.ToString()))
                {
                    var list = new List<Users>{item};
                    var usrInfo = new UserInfo(list);
                    usrInfo.ShowDialog();
                    Close();
                }
            }
            Close();
        }
    }
}
