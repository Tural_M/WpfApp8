﻿using System.Windows;

namespace WpfApp8
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register 
    {
        public Register()
        {
            InitializeComponent();
        }
       
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (textboxPass.Password.Equals(textboxConPass.Password))
            {
                 Users.Add(new Users(TextboxName.Text, TextboxSurname.Text, 
                     TextboxEmail.Text, textboxPass.ToString(), TextboxUsername.Text));
                Close();
            }
            else
            {
                MessageBox.Show("Passwords not equal");
                
            }
        }
    }
}
