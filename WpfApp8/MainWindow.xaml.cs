﻿using System;
using System.Windows;

namespace WpfApp8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Register _reWindow;
        UserLog _userLog;
      
        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            _userLog = new UserLog {Owner = this};
            _userLog.ShowDialog();

        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            _reWindow = new Register {Owner = this};
            _reWindow.ShowDialog();
            _reWindow.Closed += ReWindow_Closed;
        }

        private void ReWindow_Closed(object sender, EventArgs e)
        {
            _reWindow = null;
        }
    }
}
