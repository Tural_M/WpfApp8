﻿using System.Collections.Generic;
using System.Windows;


namespace WpfApp8
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Interaction logic for UserInfo.xaml
    /// </summary>
    public partial class UserInfo 
    {
        public UserInfo(IEnumerable<Users> userList) 
        {
            InitializeComponent();
            UserData.ItemsSource = userList;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var photoviewer = new Photoviewer();
            photoviewer.ShowDialog();
        }
    }
}
