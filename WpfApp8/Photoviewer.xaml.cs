﻿using System.Windows;
using System.IO;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace WpfApp8
{
    /// <inheritdoc>
    ///     <cref></cref>
    ///    </inheritdoc>
    ///     <summary>
    /// Логика взаимодействия для Photoviewer.xaml
    /// </summary>
    public partial class Photoviewer 
    {
        public Photoviewer()
        {
            InitializeComponent();
        }

        private List<string> _pathImages = new List<string>();
        Uri _uri;
        BitmapImage _bitmap;
        DirectoryInfo _path;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
        private void PathBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtBox.Text))
                {
                    _path = new DirectoryInfo(txtBox.Text);
                    FindJpg(_path);
                    AddImages();
                }
                else
                {
                    MessageBox.Show("Wrong Path");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show((ex.Message));
            }

        }

        private void AddImages()
        {

            foreach (var path in _pathImages)
            {
                _uri = new Uri(path);
                _bitmap = new BitmapImage(_uri);
                PictureWrap.Children.Add(new Image
                {
                    Margin = new Thickness(5),
                    Source = _bitmap,
                    Width = 100,
                    Height = 100
                });
            }
        }

        private void FindJpg(DirectoryInfo path)
        {
            try
            {
                foreach (FileInfo item in path.GetFiles("*.jpg"))
                {
                    _pathImages.Add(item.FullName);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            try
            {
                foreach (DirectoryInfo item in path.GetDirectories())
                {
                    FindJpg(item);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }

        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            PictureWrap.Width = zoom.Value;
            PictureWrap.Height = zoom.Value;

        }
    }

}
